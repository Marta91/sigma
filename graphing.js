
var nodes= [];




document.getElementById("baton1").addEventListener('click', function(){
var node = {};
	node["id"] = document.getElementById("id").value;
	node["label"] = document.getElementById("label").value;
	node["x"] = document.getElementById("x").value;
	node["y"] = document.getElementById("y").value;
	node["size"] = document.getElementById("size").value;
	
	var canIAddNode = true;     // flaga czy mogę dodać węzeł
    for (var i = 0; i < nodes.length; i++) {  // iterujemy przez el
        if (nodes[i].id == node.id) { // jeśli trafimy na el. o tym samym id
            canIAddNode = false;    // zaznaczamy że nie można dodać nowego węzła
            break;
        }
    }
    if (canIAddNode) {      // to przejdzie tylko jeśli sprawdzenie
        nodes.push(node);  // wyżej nie napotkało węzła o tym samym id
        console.log(nodes);
        draw();
    }else{
        //wyświetlić komunikat
    }
 
 console.log(nodes);
});


var edges= [];

var s;

document.getElementById("baton2").addEventListener('click', function(){
var edge = {};
    edge["id"] = document.getElementById("id2").value;
    edge["source"] = document.getElementById("source").value;
    edge["target"] = document.getElementById("target").value;

    var canIAddEdge = true;     // flaga czy mogę dodać węzeł
    for (var i = 0; i < edges.length; i++) {  // iterujemy przez el
        if (edges[i].id == edges.id) { // jeśli trafimy na el. o tym samym id
            canIAddEdge = false;    // zaznaczamy że nie można dodać nowego węzła
            break;
        }
    }
    if (canIAddEdge) {      // to przejdzie tylko jeśli sprawdzenie
        edges.push(edge);  // wyżej nie napotkało węzła o tym samym id
        console.log(edges);
        draw();
    }else{
        //wyświetlić komunikat
    }
 
 
 console.log(edges);
});

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

document.getElementById('anime').addEventListener('click', function () {
    var nodesToAnimate = [];
    
    for (var i = 0; i < s.graph.nodes().length; i++) {
        s.graph.nodes()[i].color = getRandomColor();
        s.graph.nodes()[i].hover_color = getRandomColor();
        s.graph.nodes()[i].to_x = Math.random()*20;
        s.graph.nodes()[i].to_y = Math.random()*20;
        
        nodesToAnimate.push(s.graph.nodes()[i].id);
    }
    
    sigma.plugins.animate(
            s,
            {
                x: 'to_x',
                y: 'to_y',
                color: 'hover_color',
            },
            {
                nodes: nodesToAnimate,
                duration: 2000,
                onComplete: function () {
                    console.log("done");
                }
            }
    );
});

document.getElementById('baton1').addEventListener('click', function(){
	draw();
});

document.getElementById('baton2').addEventListener('click', function(){
	draw();
});

function draw(){
	var graphData = {};
	graphData['nodes'] = nodes;
	graphData['edges'] = edges;
	
	
	document.getElementById('sigma-container').innerHTML = " ";
	s = new sigma({
        graph: graphData,
        renderer: {
            container: document.getElementById('sigma-container'),
            type: 'canvas'
        },
        settings: {
            doubleClickEnabled: false,
            minEdgeSize: 0.5,
            maxEdgeSize: 4,
            enableEdgeHovering: true,
            edgeHoverColor: 'edge',
            defaultEdgeHoverColor: '#000',
            edgeHoverSizeRatio: 1,
            edgeHoverExtremities: true,
        }
    });
}